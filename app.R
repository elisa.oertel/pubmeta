# PubMeta Prototype November 19
   
library(shiny)
library(shiny.semantic)
library(semantic.dashboard)
library(shinyjs)
library(shinyWidgets)
library(shinycssloaders)
library(shinyBS)  # make modal plots?
library(plotly)        
# packages for content mining
options(stringsAsFactors = FALSE)
library(stringi)
library(ggplot2)
library(tidyverse)
library(stm)
library(RColorBrewer)
library(dplyr)
library(reshape2)
library(shinydisconnect)
library(svglite)
library(svgPanZoom)



source("PM_functions.R")
source("PM_ui.R")
source("PM_server.R")

# load("pubmeta/Backend/AnimBreedGenet/No_Subset/Stopwords_default/NoStem/2005-2009/3/stm_model.RData")

# Run the application 
shinyApp(ui = ui, server = server)

