### functions behind PubMeta UI

timeoutSeconds <- 1500  # 25 min

inactivity <- sprintf("function idleTimer() {
var t = setTimeout(logout, %s);
window.onmousemove = resetTimer; // catches mouse movements
window.onmousedown = resetTimer; // catches mouse movements
window.onclick = resetTimer;     // catches mouse clicks
window.onscroll = resetTimer;    // catches scrolling
window.onkeypress = resetTimer;  //catches keyboard actions

function logout() {
Shiny.setInputValue('timeOut', '%ss')
}

function resetTimer() {
clearTimeout(t);
t = setTimeout(logout, %s);  // time is in milliseconds (1000 is 1 second)
}
}
idleTimer();", timeoutSeconds*1000, timeoutSeconds, timeoutSeconds*1000)


get_journals <- function(){
  ret <- list.dirs("/srv/shiny-server/pubmeta/Backend/", recursive = F, full.names = F)
  return(ret)
}

get_journal_shortname <- function(jour){
  ret <- readLines(paste0("/srv/shiny-server/pubmeta/Backend/", jour, "/shortname.txt"), warn = F)
  return(ret)
}

create_journal_menuItem <- function(jour){
  return(menuItem( tabName = jour , get_journal_shortname(jour) ))
}

create_journal_tab <- function(jour){
 return(tabItem(tabName = jour,
                fluidRow(
                  valueBoxOutput(paste0("abstracts_", jour)),
                  valueBoxOutput(paste0("authors_", jour)),
                  valueBoxOutput(paste0("countries_", jour))
                ),

		 fluidRow(id=paste0("modbox1_",jour),
                box(
                    width=15, height=10, color="teal",
                    title= "topic development in time",
		    tags$div(style="display:inline-block;vertical-align:top;", selectInput(paste0("timespan_",jour),label="Selected time span", "")),
		    tags$div(style="display:inline-block;vertical-align:top;float:right;", sliderInput(paste0("k_",jour), "Amount of topics", min=3, max=15, value=3, ticks=F)),
                    withSpinner(plotlyOutput(paste0("stm_model_", jour)))
                ) # end box
                ),
                 fluidRow(id=paste0("modbox2_",jour),
                box(
                    width=15, height=10,
                    title = "topic keywords",
                    color="teal",
                    checkboxInput( paste0("stem_",jour) , "Calculate model with word stemming"),
                    htmlOutput( paste0("stm_topics_", jour))
                 ) # end box
              ),


                 fluidRow(id=paste0("statbox1_",jour),
	    	tabBox(
                #width = 7 ,
                #height = 9,
		color="teal",
                title = "publication overview",
		#tags$div(HTML("&nbsp;")),
		#tags$div(HTML("&nbsp;")),
                tabs = list(
			    list(menu = "time span", content = list( column(12, align = "center", withSpinner(plotOutput(paste0("stats_abstracts_", jour)))) # ,
								  #  selectInput(paste0("timespan_",jour),label=NULL, "")
								    )
			    ), # end first menu
			    list(menu = "countries", content = list("Coming soon...")
				 ) # end second menu
			    ) # end tabs
                ), # end tabbox 
                tabBox(
              #  width = 8, heig
                title = "top authors publishing", color="teal",
		tabs = list(
			    list(menu = "authors in total", content = list(
								  column(12, align = "center", withSpinner(plotOutput(paste0("stats_experts_", jour)))),
                						  chooseSliderSkin("Nice"), setSliderColor("DimGray", 1),
						                  sliderInput(paste0("slide_k_",jour)," ", min = 5, max = 50, step = 5, value = 10, ticks = F)
		)
		), # end first menu
               		    list(menu = "authors per topic", content = list( withSpinner(svgPanZoomOutput(paste0("experts_", jour))),
									 tags$div(HTML("&nbsp")) 
									 # actionButton("modPlot", "Enlarge")
									 # bsModal("modPlot", "Experts", "show modal", size = "large", plotOutput(paste0("experts_", jour)))
								  #  column(12, align="center", "Note: It will cause some trouble, if there is no author with more than one publication in a topic. For now, the plot above will be empty in the regarding section." )
		)
		) # end second menu
	 	) # end tabs
                ) # end tabbox
              ) #  ), 
             #    fluidRow(id=paste0("modbox1_",jour),
             #   box(
#		    width=15, height=10, 
 #                   title= "topic development in time", 
#		    sliderInput(paste0("k_",jour), "Amount of topics", min=3, max=15, value=3),
 #                   withSpinner(plotlyOutput(paste0("stm_model_", jour)))
#		) # end box
 #               ),               
  #               fluidRow(id=paste0("modbox2_",jour),
   #             box(
#		    width=15, height=10, 
 #                   title = "topic keywords",
#		    color="teal",
#		    checkboxInput( paste0("stem_",jour) , "Calculate model with word stemming"),
 #                   htmlOutput( paste0("stm_topics_", jour))
  #               ) # end box
   #           )
          )
 )
}


get_model <- function(tab, timespan, jour_subset = "No_Subset", jour_stopwords = "Stopwords_default",  stemming = "NoStem", k=3){
  return(paste0("/srv/shiny-server/pubmeta/Backend/", tab, "/", jour_subset, "/", jour_stopwords, "/", stemming, "/", timespan, "/", k, "/stm_model.RData"))
}

get_experts_data <- function(tab, timespan, jour_subset = "No_Subset", jour_stopwords = "Stopwords_default",  stemming = "NoStem", k=3){
  return(paste0("/srv/shiny-server/pubmeta/Backend/", tab, "/", jour_subset, "/", jour_stopwords, "/", stemming, "/", timespan, "/", k, "/experts_data.RData"))
}

get_stats <- function(out){
  stats <- list()
  # get the amount of abstracts from meta-information
  stats[[1]] <- length(out$meta$abstract)
  # get unique authors per journal
  all_authors <- unlist(stri_split_fixed(out$meta$authors, pattern = "|"))
  stats[[2]] <- length(unique(all_authors))
  # get participting countries
  all_countries <- unlist(stri_split_fixed(out$meta$affiliation, pattern = "|"))
  stats[[3]] <- length(unique(all_countries))
  # get data to create barplot
  pubdate <- as.data.frame(table(out$meta$year), stringsAsFactors = F)
  pubdate$Var1 <- as.numeric(pubdate$Var1)
  names(pubdate) <- c("year of publication", "frequency")
  stats[[4]] <- pubdate
  # create expert-overview
  tmp <- as.data.frame(table(all_authors), stringsAsFactors = F)
  tmp <- tmp[order(tmp[, 2], decreasing = T), ]
  stats[[5]] <- tmp
  return(stats)
}

get_model_timeplot <- function(stm_model, out){
  
  docs_theta <- stm_model$theta
  docs_years <- out$meta$year
  combi <-cbind.data.frame(docs_years, docs_theta)
  combi$docs_years <- as.factor(combi$docs_years)
  topic_mean <- combi %>% group_by(docs_years) %>% summarise_if(is.numeric, mean)
  d <- melt(topic_mean, id.vars = "docs_years")
  names(d) <- c("years","topic","topic_prop")
  
  p <- ggplot(d, aes(x=years, y=topic_prop, color=topic, group=topic))+ geom_smooth(span=0.4, se=F) + geom_line(alpha=0.2, show.legend=F) + ylab("estimated topic proportion") +
         xlab("year of publication") + labs(color="topic") + theme_classic() +  theme(axis.text.x = element_text(angle = 45), legend.title = element_blank())

 return(p) # + guides(color = guide_legend(override.aes = list(alpha = 0.9)))  also tried: guides(color = guide_legend(override.aes = list(size = 3) ) ) + geom_smooth(span = 0.4 ,se=F)  - NOT WORKING ?? Plotly-Error?
  
}

get_experts_radialplot <- function(data, sf){

	# reorder labels numerically
	data$group <- factor(data$group, levels = paste(1:nlevels(data$group)))

# the following code is taken from the R Graph Gallery and slightly adjusted

	# Set a number of 'empty bar' to add at the end of each group
	empty_bar=3
	to_add = data.frame( matrix(NA, empty_bar*nlevels(data$group), ncol(data)) )
	colnames(to_add) = colnames(data)
	to_add$group=rep(levels(data$group), each=empty_bar)
	data=rbind(data, to_add)
	data=data %>% arrange(group)
	data$id=seq(1, nrow(data))

	# Get the name and the y position of each label
	label_data=data
	number_of_bar=nrow(label_data)
	angle= 90 - 360 * (label_data$id-0.5) /number_of_bar     # I substract 0.5 because the letter must have the angle of the center of the bars. Not extreme right(1) or extreme left (0)
	label_data$hjust<-ifelse( angle < -90, 1, 0)
	label_data$angle<-ifelse(angle < -90, angle+180, angle)

	# prepare a data frame for base lines
	base_data=data %>%
	  group_by(group) %>%
	  summarize(start=min(id), end=max(id) - empty_bar) %>%
	  rowwise() %>%
	  mutate(title=mean(c(start, end)))

	# prepare a data frame for grid (scales)
	grid_data = base_data
	grid_data$end = grid_data$end[ c( nrow(grid_data), 1:nrow(grid_data)-1)] + 1
	grid_data$start = grid_data$start - 1
	grid_data=grid_data[-1,]

	max_hjust <- rep(c(1,0),10)
return( ggplot(data, aes(x=as.factor(id), y=value, fill=group)) + 
  geom_bar(aes(x=as.factor(id), y=value, fill=group), stat="identity", alpha=0.5) +
  
  # Add a val=100/75/50/25 lines. I do it at the beginning to make sur barplots are OVER it.
  geom_segment(data=grid_data, aes(x = end, y = sf, xend = start, yend = sf), colour = "grey", alpha=1, size=0.3 , inherit.aes = FALSE ) +
  geom_segment(data=grid_data, aes(x = end, y = 0.75*sf, xend = start, yend = 0.75*sf), colour = "grey", alpha=1, size=0.3 , inherit.aes = FALSE ) +
  geom_segment(data=grid_data, aes(x = end, y = 0.5*sf, xend = start, yend = 0.5*sf), colour = "grey", alpha=1, size=0.3 , inherit.aes = FALSE ) +
  geom_segment(data=grid_data, aes(x = end, y = 0.25*sf, xend = start, yend = 0.25*sf), colour = "grey", alpha=1, size=0.3 , inherit.aes = FALSE ) +
  
  # Add text showing the value of each 100/75/50/25 lines
  annotate("text", x = rep(max(data$id),4), y = c(0.25*sf, 0.5*sf, 0.75*sf, sf), label = paste(c(0.25, 0.5, 0.75, 1)* sf) , color="grey", size=4 , angle=0, fontface="bold", hjust=1) +
  
  geom_bar(aes(x=as.factor(id), y=value, fill=group), stat="identity", alpha=0.5) +
  ylim(-sf , sf+0.1*sf ) +
  theme_minimal() +
  theme(
    legend.position = "none",
    axis.text = element_blank(),
    axis.title = element_blank(),
    panel.grid = element_blank(),
    plot.margin = unit(rep(-3,4), "cm") 
  ) +
  coord_polar() + 
  geom_text(data=label_data, aes(x=id, y=value+(sf/10), label=individual, hjust=hjust), color="black",alpha=0.6, size=3.5, angle= label_data$angle, inherit.aes = FALSE ) +  # fontface="bold"
  
  # Add base line information
 #  geom_segment(data=base_data, aes(x = start, y = -5, xend = end, yend = -5), colour = "black", alpha=0.8, size=0.6 , inherit.aes = FALSE )  +
  geom_text(data=base_data, aes(x = title, y = -(sf/5), label=paste("#",group)), hjust=c( max_hjust[1:length(base_data$group)]  ), colour = "black", alpha=0.8, size=3, fontface="bold", inherit.aes = FALSE)

       )
}
