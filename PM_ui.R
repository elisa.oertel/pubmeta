ui <- dashboardPage(
#		    tags$head(HTML(<script type="text/javascript">
#  var _paq = window._paq = window._paq || [];
#  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
#  _paq.push(['trackPageView']);
#  _paq.push(['enableLinkTracking']);
#  (function() {
#    var u="https://matomo.gwdg.de/";
#    _paq.push(['setTrackerUrl', u+'piwik.php']);
#    _paq.push(['setSiteId', '406']);
#    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
#    g.type='text/javascript'; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
#  })();
#</script>
#				   )
#			      ),
  
  dashboardHeader( disable=TRUE ),
  
  dashboardSidebar( size = "thin", color="teal",
    sidebarMenu(id="tabs",

      tags$div(HTML("&nbsp;")),
      menuItem(tabName = "home", text="PubMeta", icon = icon("home")),
      menuItem(tabName=  "idea", text="Idea", icon = icon("lightbulb")),
      menuItem(tabName= "impressum", text="Impressum"),

      tags$div( HTML("&nbsp;")),
      tags$hr(),
      lapply(get_journals(), create_journal_menuItem),
      
      tags$div( HTML("&nbsp;"), style="height:8vh;"),
      tags$hr(),
      tags$div( downloadButton("mod_down", "Download current model selection (RData)"), style="padding-left:16px;" )
      
    ) # end menu
  ), # end sidebar
  
  dashboardBody(
		useShinyjs(),
		tags$script(inactivity),
		disconnectMessage(
    		text = "We are sorry - Your session has timed out.",
		    refresh = "",
		    background = "#646464e6",
		    size = 36,
		    width = "full",
		    top = "center",
		    colour = "white",
		    overlayColour = "#999",
		    overlayOpacity = 0.4
		  ),

     tags$head(tags$meta(name="description", content="free online topic modelling and topic development tool in scientific journals"),
               tags$meta(name="author", content="elisa oertel"),
               tags$meta(name="application-name", content="pubmeta"),
	       tags$meta(name="google-site-verification", content="S5pGqYMnuI5D4uLwekVxyhebkK7kFWNcfUfVZIa66S4"),
               tags$meta(name="keywords", content="pubmeta, oertel, topic modelling, R, shiny, R shiny, text mining, pubmed, journal analysis, uni goettingen, meta analysis, STM"),

	       # websocket script
	       HTML(
          "
          <script>
          var socket_timeout_interval
          var n = 0
          $(document).on('shiny:connected', function(event) {
          socket_timeout_interval = setInterval(function(){
          Shiny.onInputChange('count', n++)
          }, 15000)
          });
          $(document).on('shiny:disconnected', function(event) {
          clearInterval(socket_timeout_interval)
          });
          </script>
          "
        ),

	       # matomo script
	HTML(
                
                "   <script type=\"text/javascript\">
            var _paq = window._paq = window._paq || [];
        
            _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u='https://matomo.gwdg.de/';
            _paq.push(['setTrackerUrl', u+'piwik.php']);
            _paq.push(['setSiteId', '406']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
        </script>
        "
                
            )),
    
    tabItems(
    
      tabItem(tabName = "home",
            fluidRow(
              box(width=15, color="teal",
                title = "Welcome", column(12, align="center",
                tags$h2("PubMeta is an online Content Mining Tool to inspect topic development in scientific journals."), br(), " All publication information is retrieved from", tags$a("PubMed", href="https://pubmed.ncbi.nlm.nih.gov/", target="_blank"),
		tags$i("via"), tags$a("Entrez direct", href="https://www.ncbi.nlm.nih.gov/books/NBK179288/", target="_blank"), "and data mining algorithms are implemented in R.", br(),
		"So far, you can answer the following questions with PubMeta:",
	       tags$ul( 
		       tags$li("Which topics emerged or got less important over time?"), 
		       tags$li("What are the keywords defining these topics?"),	
                       tags$li("Who are the top authors for a given time span in the selected journal?"),
		       tags$li("Who are the top authors per topic in the selected journal?")),
		br(),
		"Until now, there are only a few journals available as proof-of-concept, but more journals are to come (for suggestions you are welcome to contact Elisa, so far, only the most important journals to our research are included).", br(),"Current journals are: ", tags$i("Journal of Animal Breeding and Genetics, Appetite, Bioinformatics, Food Chemistry,  Meat Science, AAAS Science"), br(),br(),
		"PubMeta is part of a PHD-Project and therefore ongoing work. It will be extended over time and you are welcome to send suggestions. To contact the programmer, please write an eMail to ", tags$a("Elisa Oertel", href="mailto:elisa.oertel@uni-goettingen.de?subject=Feedback_PubMeta"), br()
              ))
            ),
            fluidRow(
              box(width=15, color="teal",
                title = "First Steps",
                "To get started, simply click on a journal in the menu, you may then:",
                br(),br(),
                "1) Explore basic statistics about this journal",
                br(),
                "2) Set the amount of topics you want to calculate",
                br(),
                "3) Explore topics per time span and investigate word sets belonging to the respective topics",
		br(),
		"4) Download the full model corresponding to your setup for local use"
              ) # end box
            )  # end Fluid Row
            ), # end tabItem

	     tabItem(tabName="idea",
		      fluidRow(
                       box(width=10, height=15, color="teal",
                           title="the Idea behind PubMeta",
                           column(8, align="center",
                           imageOutput("idea_image"))
                           )
                       )
		     ),

	     tabItem(tabName="impressum",
		     fluidRow( tags$h4("All content on this website was created by:"), br(), tags$p("Elisa Oertel, Department of Animal Science, University of Goettingen, Albrecht-Thaer-Weg 3, 37075 Goettingen, Germany."), br(), br()," Tel: +49 551 39 25621 and ", HTML("&nbsp;"), tags$a("official website", href ="https://www.uni-goettingen.de/en/599796.html", target="_blank" )),
		    #  textOutput("keepAlive"),
		     fluidRow( tags$iframe(src="https://matomo.gwdg.de/index.php?module=CoreAdminHome&action=optOut&language=de",style="border: 0; height: 200px; width: 600px;")
		     )
		     ),
 
      # create journal tabNames for all menuItems (that will be created by folder names found in Backend/) 
      lapply(get_journals(), create_journal_tab)
      
    ) # end TabItems
    ) #, theme = "journal" # end body

    
  ) # end of Page
  
