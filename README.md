# Welcome to PubMeta!

PubMeta is the first prototype of an online content mining tool to inspect **topic development in scientific journals**. It is based on the **R** package *STM* ([see official website](https://www.structuraltopicmodel.com/)) and uses [PubMed](https://pubmed.ncbi.nlm.nih.gov/) database as data source. 

To see PubMeta in action, please visit us at: www.pubmeta.uni-goettingen.de

If you have any questions, please feel free to contact me at: *elisa.oertel@uni-goettingen.de*
