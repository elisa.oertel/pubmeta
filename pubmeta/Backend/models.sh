#!/bin/bash

JOURNALS=`ls -d */`
for J in $JOURNALS; do
	echo $J
	cd $J
	Rscript ../models.R	
	cd ..
done
