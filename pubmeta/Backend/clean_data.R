## This script will be run by esearch.sh

library(stringi)
options(scipen = 999)
#################################
# functions needed in this script

read_data <- function(){
  
  data_years <- readLines("years.txt", encoding = "UTF-8")
  data_affil <- readLines("affil.txt", encoding = "UTF-8")
  data_authors <- readLines("authors.txt", encoding = "UTF-8")
  data_seniors <- readLines("authors_last.txt", encoding = "UTF-8")
  data_juniors <- readLines("authors_first.txt", encoding = "UTF-8")
  data_abstr <- readLines("abstracts.txt", encoding = "UTF-8")
  
  return(list(data_years,data_affil,data_authors,data_seniors, data_juniors,data_abstr))
}

creat_mdf <- function(chr_lines){
  m <- matrix(NA, nrow = length(chr_lines), ncol=2)
  for(i in 1:dim(m)[1]){
    for(j in 1:dim(m)[2]){
      m[i,j] <- unlist(strsplit(chr_lines[i], "\t"))[j]
    }
  }
  return(as.data.frame(m, stringsAsFactors = F))
}

make_df <- function(x){
  for(i in 1:length(x)){
    x[[i]] <- creat_mdf(x[[i]])  
  }
  return(x)
}

merge_df <- function(x){
  tmp <- merge(x[[1]], x[[2]], by="V1")
  for(i in 3:length(x)){
    tmp <- suppressWarnings(merge(tmp, x[[i]], by="V1"))
  }
  names(tmp) <- c("ID", "year", "affiliation","authors", "senior", "junior", "abstract")
  tmp$ID <- as.numeric(tmp$ID)
  tmp$year <- as.numeric(tmp$year)
  if(length(unique(tmp$ID)) == length(tmp$ID)) {print("ID checked")}
  raus <- which(tmp$year > 2050 )   # exclude data only containing ID
  if(length(raus) >= 1 ) { tmp <- tmp[-c(raus),] }
  return(tmp)
}

country_database <- function(){
  country <- readLines("../countries.txt")
  # adjust some information / synonyms
  country[7] <- "Antigua"
  country[24] <- "Bosnia"
  country[67] <- "Gambia"
  country[87] <- "Israel"
  country[89] <- "Ivory Coast"
  country[95] <- "North Korea"
  country[96] <- "South Korea"
  country[99] <- "Kyrgyzstan"
  country[120] <- "Moldova"
  country[127] <- "Myanmar"
  country[127] <- "Macedonia"
  country[163] <- "Slovakia"
  country[190] <- "USA"
  country <- country[-c(40,44,53,134,195,196)]
  
  
  # additionally match and maybe convert later:
  country <- c(country, "Barbuda", "Herzegowina","Herzegovina", "Korea", "UK", "U.K", "México", "U.S.A", "United States of America", "España",
               "Neuseeland", "Deutschland", "Scotland", "Ireland", "Taiwan", "Nebraska", "California", "Hong Kong", "Macau", "Burma",
               "PR China", "Russia", "Italia", "United Kingdom", "England", "US", "Kyrgyz Republic", "Republic of China", "PRC",
               "Brasil", "Beijing", "Algérie", "Bénin", "Somaliland")
  
  for(i in 1:length(country)){
    country[i] <- paste0(country[i],".")
  }
  # country <- c(country, "[A-Z]{2}\\s[0-9]{5}\\.", "[A-Z][^UK]\\.$")
  return(country)
}

rename_country <- function(x){
  for(i in 1:length(x)){
    x[i] <- sub("Italia.", "Italy.", x[i], fixed = T)
    x[i] <- sub("United Kingdom.", "UK.", x[i], fixed = T)
    x[i] <- sub("England.", "UK.", x[i], fixed = T)
    x[i] <- sub("México.", "Mexico.", x[i], fixed = T)
    x[i] <- sub("Antigua.", "Antigua/Barbuda.", x[i], fixed = T)
    x[i] <- sub("Barbuda.", "Antigua/Barbuda.", x[i], fixed = T)
    x[i] <- sub("Bosnia.", "Bosnia/Herzegowina.", x[i], fixed = T)
    x[i] <- sub("Herzegowina.", "Bosnia/Herzegowina.", x[i], fixed = T)
    x[i] <- sub("Herzegovina.", "Bosnia/Herzegowina.", x[i], fixed = T)
    x[i] <- sub("U.S.A.", "USA.", x[i], fixed = T)
    x[i] <- sub("US.", "USA.", x[i], fixed = T)
    x[i] <- sub("United States of America.", "USA.", x[i], fixed = T)
    x[i] <- sub("California.", "USA.", x[i], fixed = T)
    x[i] <- sub("Nebraska.", "USA.", x[i], fixed = T)
    x[i] <- sub("Colombia.", "USA.", x[i], fixed = T)
    x[i] <- sub("Brasil.", "Brazil.", x[i], fixed = T)
    x[i] <- sub("Beijing.", "China.", x[i], fixed = T)
    x[i] <- sub("Algérie.", "Algeria.", x[i], fixed = T)
    x[i] <- sub("Bénin.", "Benin.", x[i], fixed = T)
    x[i] <- sub("Somaliland.", "Somalia.", x[i], fixed = T)
    # because some Americans only use the abbrevation for their state
    # gsub("[A-Z]{2}\\s[0-9]{5}\\.", "USA.", x[i])
    #  gsub("[A-Z][^UK]\\.$", "USA.", x[i])    # that does not work as well, since emails etc. will be also grep-ed
  }
  return(x)
}

##############################################################################################

# read data by list elements: year, affil, authors, senior, junior, abstract
df <- read_data()

# split list elements by \t and convert list elements to dataframe
df <- make_df(df)

# check for NAs only in first dataframe, since this one is used as starting point for merge later
raus <- which(is.na((df[[1]][2])))
if(length(raus) >= 1){ 
  for(i in 1:length(df)){
    df[[i]] <- df[[i]][-c(raus) ,]
  }}
rm(raus)

# merge by ID  (returns "ID checked" if there are only unique IDs)
df <- merge_df(df)

# which absctract texts are actually comments or editorials or alike
raus <- which(is.na(df$abstract)) 
if(length(raus) >= 1){ df <- df[-c(raus),] }
rm(raus)

# compare affiliation to country database
country <- country_database()

for(m in 1:length(df$affiliation)){
  split <- unlist(stri_split_fixed(df$affiliation[m], "|"))
  for(n in 1:length(split)){
    tmp <- country[which(stri_detect_fixed(split[n], country))]
    if(length(tmp) == 1) {split[n] <- tmp}
    else{split[n] <- "NR."}
  }
  df$affiliation[m] <- paste(split, collapse = "|")
}

df$affiliation <- rename_country(df$affiliation)

save(df, file="data_cleaned.RData")  
