#!/bin/bash

JOURNALS=`ls -d */`
for J in $JOURNALS; do
	echo $J
	cd $J
	FULLNAME=`cat fullname.txt`	
	echo $FULLNAME
	esearch -db pubmed -query "$FULLNAME" | efetch -format xml > fulljournal.xml
	xtract -input fulljournal.xml -pattern PubmedArticle -element MedlineCitation/PMID -year "PubDate/*" > years.txt
	xtract -input fulljournal.xml -pattern PubmedArticle -element MedlineCitation/PMID -block Author -sep " " -tab "|" -element LastName,Initials > authors.txt
	xtract -input fulljournal.xml -pattern PubmedArticle -element MedlineCitation/PMID -block Author -position last -sep " " -element LastName,Initials > authors_last.txt
	xtract -input fulljournal.xml -pattern PubmedArticle -element MedlineCitation/PMID -block Author -position first -sep " " -element LastName,Initials > authors_first.txt
	xtract -input fulljournal.xml -pattern PubmedArticle -element MedlineCitation/PMID, AbstractText > abstracts.txt
	xtract -input fulljournal.xml -pattern PubmedArticle -element MedlineCitation/PMID -sep "|" -element Affiliation > affil.txt

	Rscript ../clean_data.R
	cd ..
done
